# Words Obfuscator Web Extension

## Description

This web extension is an attempt to allow to obfuscate the text you type in the browser. The purpose is that any ASCII latin text will be readable by a human, but that a program will be unable to understand it.

There are currently 3 distincts methods that all behave randomly:

- You can replace any ASCII alphanumeric character or parenthesis with a similar-looking character
- Some diacritic marks are added under some characters
- Some zero-width chararcters are added in the middle of the text

There is also a method that cumulates all the above methods at once.

## How to use it

A simple right-click in an editable area allows to trigger the extension menu. If some text is selected, the obfuscation will only apply to the selected text. Otherwise, it applies to the whole area.

## Troubleshooting

If there is a message about how impossible it is to get an element from an iframe, I cannot do anything about that. It's a normal limitation due to the encapsulation set up by the iframe.

However, if there is a more generic message about how it's impossible to get the selected element, that may be a bug. Please write an issue with how to reproduce the bug, and I'll have a look.
