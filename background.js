/* Server side. */

browser.menus.create({
    id: "world-obfuscator-submenu",
    title: browser.i18n.getMessage("menuItemMain"),
    contexts: ["editable"]
});

function executeContentWithMessage(msg) {
    return (info, tab) => browser.tabs.executeScript(tab.id, { file: 'content-script.js' }, () => {
        console.debug("[Words Obfuscator]", "Message sent:", info);
        browser.tabs.sendMessage(tab.id, {
            elemId: info.targetElementId,
            text: info.selectionText,
            method: msg,
        })
    })
}

browser.menus.create({
    parentId: "world-obfuscator-submenu",
    title: browser.i18n.getMessage("menuItemWithAnotherAlphabet"),
    contexts: ["editable"],
    onclick: executeContentWithMessage("anotherAlphabet"),
});

browser.menus.create({
    parentId: "world-obfuscator-submenu",
    title: browser.i18n.getMessage("menuItemWithDiacritic"),
    contexts: ["editable"],
    onclick: executeContentWithMessage("diacritic"),
});

browser.menus.create({
    parentId: "world-obfuscator-submenu",
    title: browser.i18n.getMessage("menuItemWithInvisibleSpace"),
    contexts: ["editable"],
    onclick: executeContentWithMessage("invisibleSpace"),
});

browser.menus.create({
    parentId: "world-obfuscator-submenu",
    title: browser.i18n.getMessage("menuItemWithMultiple"),
    contexts: ["editable"],
    onclick: executeContentWithMessage("multiple"),
});
