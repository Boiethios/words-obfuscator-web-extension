(function () {
    if (window.done) {
        return;
    }
    window.done = true;
    const debug = false;

    /**
     * Get a random element from an array.
     */
    Array.prototype.rand_elem = function () {
        return this[Math.floor(Math.random() * this.length)];
    };

    /**
     * Custom logging functions.
     */
    const out = {
        debug: function (...args) { return console.debug("[Words Obfuscator]", ...args); },
        log: function (...args) { return console.log("[Words Obfuscator]", ...args); },
        warn: function (...args) { return console.warn("[Words Obfuscator]", ...args); },
        error: function (...args) { return console.error("[Words Obfuscator]", ...args); },
    };

    /**
     * Wrapper for any editable element to behave the same:
     * 
     *                 | VALUE     | SELECTION
     * ---------------------------------------------------------
     * input           | value     | selectionStart/selectionEnd
     * textarea        | value     | selectionStart/selectionEnd
     * contenteditable | innerHTML | window.getSelection()
     */
    function Editable(elem) {
        const tag = elem.nodeName.toLowerCase();
        const isFormTag = tag === 'input' || tag === 'textarea';

        return {
            get value() {
                return isFormTag ? elem.value : elem.innerHTML;
            },

            set value(text) {
                if (isFormTag) {
                    elem.value = text;
                } else {
                    elem.innerHTML = text;
                }
            },

            get selStart() {
                if (isFormTag) {
                    return elem.selectionStart;
                } else {
                    const sel = window.getSelection();
                    return Math.min(sel.anchorOffset, sel.focusOffset);
                }
            },

            get selEnd() {
                if (isFormTag) {
                    return elem.selectionEnd;
                } else {
                    const sel = window.getSelection();
                    return Math.max(sel.anchorOffset, sel.focusOffset);
                }
            },
        }
    }

    /**
     * Custom in-page notification.
     */
    class Notify {
        constructor(text, color) {
            const notifier = document.createElement("div");

            notifier.style.position = "fixed";
            notifier.style.width = "100%";
            notifier.style.lineHeight = "1.5em";
            notifier.style.top = "0";
            notifier.style.zIndex = "99999";
            notifier.style.fontSize = "2em";
            notifier.style.textAlign = "center";
            notifier.style.backgroundColor = color;

            notifier.innerText = "Words Obfuscator: " + text;

            document.body.appendChild(notifier)

            setTimeout(() => document.body.removeChild(notifier), 1500);
        }

        static error(text) {
            out.error(text);
            new Notify(text, "red")
        }

        static success(text) {
            out.log(text);
            new Notify(text, "green")
        }
    }

    /**
     * Pseudo-random bool with a more and more probable "true" if it hasn't been out.
     */
    function RandBoolGenerator() {
        return {
            next() {
                if (this.weight === undefined) {
                    // Always returns true the first time.
                    this.weight = 3;
                    return true;
                }

                const rnd = Math.floor(Math.random() * 7); // 0~4
                const result = rnd < this.weight;

                this.weight = result ? Math.max(this.weight - 2, 0) : Math.min(this.weight + 1, 7);

                return result;
            }
        }
    }

    /**
     * Do the actual extension job.
     * 
     * @param {string} method The method used to obfuscate the text.
     * @param {Object} elem The element where the text the be replaced is.
     * @param {string} text The text to be replaced.
     */
    function obfuscate(method, rawElem, text) {
        const func = {
            "anotherAlphabet": anotherAlphabet,
            "invisibleSpace": invisibleSpace,
            "diacritic": diacritic,
            "multiple": multiple,
        }[method];
        const elem = new Editable(rawElem);
        const diacritics_table = ["\u0323", "\u0345"];
        const zero_width_table = ["\u180E", "\u200B", "\u200C", "\u200D"];
        const table = {
            //  MATH         GREEK     CYRILLIC  ROM  MISC
            a: ["\u{1D5BA}", "α", /**/ "а", /**/ /**/ "ɑ", "⍺"],
            b: ["\u{1D5BB}", /*******/ "в", "ь", /**/ "Ꮟ", "ᑲ", "ᖯ", "Ƅ", "𝈇"],
            c: ["\u{1D5BC}", "ϲ", /**/ "с", /**/ "ⅽ",],
            d: ["\u{1D5BD}", /*******/ /*******/ "ⅾ", "ɗ", "ḋ"],
            e: ["\u{1D5BE}", "ε", "ϵ", "е", "є", /**/],
            f: ["\u{1D5BF}", "ϝ", /**/ "ғ", /**/ /**/],
            g: ["\u{1D5C0}", /*******/ /*******/ /**/ "ɢ", "ɡ"],
            h: ["\u{1D5C1}", /*******/ "н", "һ", /**/ "Ꮒ", "Һ", "ɦ"],
            i: ["\u{1D5C2}", "ί", /**/ "і", /**/ "ⅰ", "ɪ", "Ꭵ"],
            j: ["\u{1D5C3}", "ϳ", /**/ "ј", /**/ /**/ "ʝ"],
            k: ["\u{1D5C4}", "κ", /**/ "к", /**/ /**/ "ԟ"],
            l: ["\u{1D5C5}", /*******/ /*******/ "ⅼ", "ɭ", "ʟ"],
            m: ["\u{1D5C6}", /*******/ "м", /**/ "ⅿ", "ጠ"],
            n: ["\u{1D5C7}", /*******/ /*******/ /**/ "ɴ", "ո", "ᥒ"],
            o: ["\u{1D5C8}", "ο", /**/ "о", /**/ /**/ "օ", "ø", "𝚘"],
            p: ["\u{1D5C9}", "ρ", /**/ "р", /**/ /**/],
            q: ["\u{1D5CA}", /*******/ /*******/ /**/ "ԛ", "զ", "գ"],
            r: ["\u{1D5CB}", /*******/ /*******/ /**/ "г", "ʀ"],
            s: ["\u{1D5CC}", /*******/ "ѕ", /**/ /**/],
            t: ["\u{1D5CD}", "τ", /**/ "т", /**/ /**/ "ʈ"],
            u: ["\u{1D5CE}", "υ", /**/ /*******/ /**/ "ս"],
            v: ["\u{1D5CF}", "ν", /**/ "ѵ", /**/ "ⅴ",],
            w: ["\u{1D5D0}", "ω", /**/ "ѡ", /**/ /**/ "ԝ"],
            x: ["\u{1D5D1}", /*******/ "х", /**/ "ⅹ", "ҳ"],
            y: ["\u{1D5D2}", /*******/ "у", "У", /**/],
            z: ["\u{1D5D3}", /*******/ /*******/ /**/ "ʐ", "ʑ"],
            A: ["\u{1D5A0}", "Α", /**/ "А", /**/ /**/ "Ꭺ"],
            B: ["\u{1D5A1}", "Β", /**/ "В", /**/ /**/ "Ᏼ"],
            C: ["\u{1D5A2}", "Ϲ", /**/ "С", /**/ "Ⅽ", "Ꮯ"],
            D: ["\u{1D5A3}", /*******/ /*******/ "Ⅾ", "ᑓ", "ᗞ", "Ꭰ"],
            E: ["\u{1D5A4}", "Ε", /**/ "Е", /**/ /**/ "Ꭼ"],
            F: ["\u{1D5A5}", "Ϝ", /**/ "Ғ", /**/ /**/],
            G: ["\u{1D5A6}", /*******/ "Ԍ", /**/ /**/ "Ꮐ", "Ꮆ"],
            H: ["\u{1D5A7}", "Η", /**/ "Н", /**/ /**/ "Ꮋ"],
            I: ["\u{1D5A8}", /*******/ "Ӏ", /**/ "Ⅰ", "Ꮖ"],
            J: ["\u{1D5A9}", "Ϳ", /**/ "Ј", /**/ /**/ "Ꭻ"],
            K: ["\u{1D5AA}", "Κ", /**/ "К", /**/ /**/ "Ꮶ"],
            L: ["\u{1D5AB}", /*******/ /*******/ "Ⅼ", "Ꮮ"],
            M: ["\u{1D5AC}", "Μ", /**/ "М", /**/ "Ⅿ", "Ꮇ"],
            N: ["\u{1D5AD}", "Ν", /**/ /*******/ /**/],
            O: ["\u{1D5AE}", "Ο", /**/ "О", /**/ /**/],
            P: ["\u{1D5AF}", "Ρ", /**/ "Р", /**/ /**/ "Ꮲ"],
            Q: ["\u{1D5B0}", /*******/ "Ԛ", /**/ /**/ "ⵕ", "Ⴍ"],
            R: ["\u{1D5B1}", /*******/ /*******/ /**/ "Ꮢ", "Ꭱ"],
            S: ["\u{1D5B2}", /*******/ "Ѕ", /**/ /**/ "Ꮪ"],
            T: ["\u{1D5B3}", "Τ", /**/ "Т", /**/ /**/ "Ꭲ"],
            U: ["\u{1D5B4}", /*******/ /*******/ /**/ "ᑌ", "Ս"],
            V: ["\u{1D5B5}", /*******/ "Ѵ", /**/ "Ⅴ", "Ꮩ"],
            W: ["\u{1D5B6}", /*******/ "Ѡ", "Ш", /**/"Ꮃ", "Ꮤ"],
            X: ["\u{1D5B7}", "Χ", /**/ "Х", /**/ "Ⅹ",],
            Y: ["\u{1D5B8}", "Υ", /**/ "Ү", "ү",  /**/],
            Z: ["\u{1D5B9}", "Ζ", /**/ /*******/  /**/ "Ꮓ"],
            "(": ["⟮", "❨"],
            ")": ["⟯", "❩"],
            "0": ["\u{1D7E2}", "߀"],
            "1": ["\u{1D7E3}", "⥠"],
            "2": ["\u{1D7E4}", "ᒿ", "շ", "ᘖ"],
            "3": ["\u{1D7E5}", "З", "ȝ", "𝈆"],
            "4": ["\u{1D7E6}", "Ꮞ"],
            "5": ["\u{1D7E7}", "Ƽ"],
            "6": ["\u{1D7E8}", "Ꮾ", "ꕃ"],
            "7": ["\u{1D7E9}", "𝈒", "ᜪ"],
            "8": ["\u{1D7EA}", "Ȣ"],
            "9": ["\u{1D7EB}", "Ⳋ"],
        };

        /**
         * Returns if the char must be replaced (see @table).
         */
        function mustBeReplaced(c) {
            return c >= 'a' && c <= 'z'
                || c >= 'A' && c <= 'Z'
                || c >= '0' && c <= '9'
                || c == '('
                || c == ')'
        }

        /* OBFUSCATION METHODS */

        function anotherAlphabet(text) {
            let result = "";
            let previous;

            for (c of text) {
                if (mustBeReplaced(c)) {
                    result += c === previous
                        ? result.slice(-1)
                        : table[c].slice(1).rand_elem();
                } else {
                    result += c;
                }
                previous = c;
            }

            return result;
        }

        function diacritic(text) {
            let rng = new RandBoolGenerator();
            let result = "";

            for (c of text) {
                result += c;
                if (rng.next()) {
                    result += diacritics_table.rand_elem();
                }
            }
            return result;
        }

        function invisibleSpace(text) {
            let rng = new RandBoolGenerator();
            let result = "";

            for (c of text) {
                result += c;
                if (rng.next()) {
                    result += zero_width_table.rand_elem();
                }
            }
            return result;
        }

        function multiple(text) {
            let rng1 = new RandBoolGenerator();
            let rng2 = new RandBoolGenerator();
            let result = "";
            let previous;

            for (c of text) {
                if (mustBeReplaced(c)) {
                    result += c === previous
                        ? result.slice(-1)
                        : table[c].slice(1).rand_elem();
                } else {
                    result += c;
                }
                previous = c;
                if (rng1.next()) {
                    result += diacritics_table.rand_elem();
                }
                if (rng2.next()) {
                    result += zero_width_table.rand_elem();
                }
            }

            return result;
        }

        const start = elem.selStart;
        const end = elem.selEnd;
        if (text == null) {
            out.debug("No selection was provided. The whole editable content will be replaced: ", start, end);
            if (start != end) {
                throw "Client selection desynchronized";
            }

            elem.value = func(elem.value);

        } else {
            const count = end - start;
            const selection = elem.value.substr(start, count);
            out.debug("A selection was provided. The selection only will be replaced:", start, count, '`' + selection + '`');

            if (selection != text) {
                throw `Client selection desynchronized: \`${selection}\` vs \`${text}\``;
            }

            // When the element is a contenteditable, this line assigns to innerHTML. It's OK because
            // the only part that can cause issue is `text`, and it comes from the user. Morover,
            // `func` only replaces some chars, adds diacritic chars or zero-width ones.
            // In the other hand, assigning to innerHTML allows to use this extension on rich texts
            // because it doesn't interfer on the HTML in the contenteditable.
            const replacement = elem.value.substr(0, start) + func(text) + elem.value.substr(end);
            elem.value = replacement;
        }

        Notify.success(browser.i18n.getMessage("messageSuccess"));
    }

    /**
     * Setup the message listening from the background.
     */
    browser.runtime.onMessage.addListener(msg => {
        out.debug("Got a message:", msg);
        try {
            if (msg.elemId != null) {
                const elem = browser.menus.getTargetElement(msg.elemId);
                if (elem == null) {
                    const activeInIframe = document.activeElement && document.activeElement.nodeName.toLowerCase() === 'iframe';
                    throw browser.i18n.getMessage(activeInIframe ? "messageErrorIdIframe" : "messageErrorId");
                }
                obfuscate(msg.method, elem, msg.text);
            }
        } catch (e) {
            Notify.error(e);
        }
    })
})();